
# This is VS Code with Python Interactive Mode
# additional has to be installed python package sympy on operating system level
#    pip install sympy



# %% Initialize setup
# ==========================================================
print("Initialize sympy, variables and functions")
import sympy as sp

x = sp.symbols('x')
w1 = sp.Function('w1')(x)
w2 = sp.Function('w2')(x)
E = sp.symbols('E')
Iy = sp.symbols('Iy')
# q0 = sp.symbols('q0')
l = sp.symbols('l')
a = sp.symbols('a')
b = sp.symbols('b')
M1 = sp.symbols('M1')
M2 = sp.symbols('M2')
F = sp.symbols('F')

# integration constants
C1 = sp.symbols('C1')
C2 = sp.symbols('C2')
C3 = sp.symbols('C3')
C4 = sp.symbols('C4')



# %% Describe physics
# ==========================================================

#   Field 1
# ------------------------------------------------
print("equations field 1")
eq1 = sp.Eq( E *Iy *w1.diff(x,x) +M1, 0 )
display(eq1)
eq1m = sp.Eq( M1, F*b/l*x )
display(eq1m)

eq1 = eq1.subs(M1, eq1m.rhs)

print("solve differential equations")
sol1 = sp.dsolve(eq1,w1)
display(sol1)


print("apply w(0) = 0 ")
sol1 = sol1.subs(C1,0)
display(sol1) 



# %% Field 2
# ------------------------------------------------
print("equations field 2")
eq2 = sp.Eq( E *Iy *w2.diff(x,x) +M2, 0 )
display(eq2)
eq2m = sp.Eq( M2, F*a/l*(l-x) )
display(eq2m)

eq2 = eq2.subs(M2, eq2m.rhs)

print("solve differential equations")
sol2 = sp.dsolve(eq2,w2)
display(sp.expand(sol2))


print("apply w(l) = 0 ")
bc2 = sol2.subs(x,l)
display(bc2)

tmp = sp.solve(bc2.rhs,C2)[0]
sol2 = sol2.subs(C2,tmp)
sol2 = sp.simplify(sol2)
display(sol2)

# Test RB 2
print("test rb w2(l) = 0")
sol2.subs(x,l)



# %% insert field linking
# ==========================================================
ics2 = {
        w1.subs(x,a): w2.subs(x,a) ,
        w1.diff(x).subs(x,7).subs(7,a): w2.diff(x).subs(x,7).subs(7,a) ,
}
print("new initial conditions")
for key, value in ics2.items():
    display( sp.Eq(key, value) )

print("w1 and w2 are equal at a")
eq31 = sp.Eq( sol1.rhs.subs(x,a) ,  sol2.rhs.subs(x,a))
display(eq31)

print("w.diff is equal at a")
eq32 = sp.Eq( sol1.rhs.diff(x).subs(x,a) ,  sol2.rhs.diff(x).subs(x,a))
display(eq32)


print("solve linear equation system")
solConstant = sp.solve( (eq31,eq32), (C1,C2))

for key, value in solConstant.items():
    display( sp.Eq(key, value) )


# %% substitute integration constants
for key, value in solConstant.items():
    # display( sp.Eq(key, value) )
    sol1 = sol1.subs(key,value)
    sol2 = sol2.subs(key,value)
display(sol1)
display(sol2)



# %% Plotting the solution
# ==========================================================
print("substitute all constatants with 1:")
substitutes = {
    l: 1,
    E: 1,
    F: 1,
    Iy: 1,
    a: 0.7,
    b: 0.3,
}

sol1plot = sol1.subs(substitutes)
display(sol1plot)

sol2plot = sol2.subs(substitutes)
display(sol2plot)

print("plotting substitution equations")
sp.plot(    
        sol1plot.rhs, 
        sol2plot.rhs, 
        (x, 0, 1), 
        ylim=(0.02, 0), 
        ylabel="w(x)" 
        )



# %% END
