
# This is VS Code with Python Interactive Mode
# additional has to be installed python package sympy on operating system level
#    pip install sympy



# %% Initialize setup
# ==========================================================
print("Initialize sympy, variables and functions")
import sympy as sp

jupyter_display = False


x = sp.symbols('x')
w = sp.Function('w')(x)
E = sp.symbols('E')
Iy = sp.symbols('Iy')
q0 = sp.symbols('q0')
l = sp.symbols('l')



# %% Describe physics
# ==========================================================
eq1 = sp.Eq( E *Iy *w.diff(x,x,x,x) -q0, 0 )
if jupyter_display: display(eq1)

print("solve differential equations")
sp.dsolve(eq1,w)



# %% differential solving with initial condition system
# ==========================================================
print("set of initial conditions:")
ics = { 
        w.subs(x,0): 0,
        w.diff(x).subs(x,0): 0 ,
        w.diff(x,x).subs(x,l): 0 ,
        w.subs(x,l): 0 ,
        }
if jupyter_display:
    for key, value in ics.items():
        display( sp.Eq(key, value) )

print("solve differential equations with initial conditions")
sol1 = sp.dsolve(eq1, w, ics=ics)
if jupyter_display: display(sol1)



# %% Plotting the solution
# ==========================================================
print("substitute all constatants with 1:")
sol1plot = sol1
sol1plot = sol1plot.subs(q0,1)
sol1plot = sol1plot.subs(l,1)
sol1plot = sol1plot.subs(E,1)
sol1plot = sol1plot.subs(Iy,1)
if jupyter_display: display(sol1plot)

print("plotting substitution equations")
sp.plot(    sol1plot.rhs, 
            (x, 0, 1), 
            ylim=(0.01, 0), 
            ylabel="w(x)" 
        ) 



# %% END
